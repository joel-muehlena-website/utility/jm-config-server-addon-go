package configServerAddon

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"
	"strconv"
	"strings"

	"go.uber.org/zap"
)

type LogLevel uint8

const (
	INFO  LogLevel = iota // Log level for printing only info messages
	ERROR LogLevel = iota // Log level for printing info messages and all error messages
)

// Type which defines a config option function
type ConfigFetchOption func(*configOptions) error

// Result passed back by the fetch function
type Result struct {
	Data  []byte
	Error error
}

type configOptions struct {
	filePath   string
	fileName   string
	fileEnding string
	env        string
	isSave     bool
	savePath   string
	isLog      bool
	logLevel   LogLevel
	zap        *zap.Logger
	ctx        context.Context
	isCtx      bool
}

// Fetch the config with a direct file path.
// Overrides all other naming attributes
func WithFilePath(filePath string) ConfigFetchOption {
	return func(co *configOptions) error {
		co.filePath = filePath
		return nil
	}
}

// Fetch the config with a custom file ending.
func WithFileEnding(fileEnding string) ConfigFetchOption {
	return func(co *configOptions) error {
		co.fileEnding = fileEnding
		return nil
	}
}

//The name of the file to fetch
func WithFileName(fileName string) ConfigFetchOption {
	return func(co *configOptions) error {
		co.fileName = fileName
		return nil
	}
}

//The env of the file to fetch. Will be appended between the filename and the file ending.
// E.g. dev or prod
func WithEnv(envName string) ConfigFetchOption {
	return func(co *configOptions) error {
		co.env = envName
		return nil
	}
}

//Saves the returned data to the given path
func WithSave(savePath string) ConfigFetchOption {
	return func(co *configOptions) error {
		co.isSave = true
		co.savePath = savePath
		return nil
	}
}

//Enables the log output
func WithLogging(lvl LogLevel) ConfigFetchOption {
	return func(co *configOptions) error {
		co.isLog = true
		co.logLevel = lvl
		return nil
	}
}

//Logs with a provided zap logger
func WithZap(zap *zap.Logger, lvl LogLevel) ConfigFetchOption {
	return func(co *configOptions) error {
		co.isLog = true
		co.logLevel = lvl
		co.zap = zap
		return nil
	}
}

//Cretae http requets with context
func WithContext(ctx context.Context) ConfigFetchOption {
	return func(co *configOptions) error {
		co.ctx = ctx
		co.isCtx = true
		return nil
	}
}

type gitlabResponseJSON struct {
	ConfigB64Data string `json:"configB64Data"`
	FileEnding    string `json:"fileEnding"`
}

// Fetch the config from the provided config server url
func FetchConfig(configServerUrl string, c chan<- Result, options ...ConfigFetchOption) {
	errorAbort := func(err error, c chan<- Result, opts *configOptions) {
		if opts.isLog && opts.logLevel == ERROR {
			if opts.zap != nil {
				opts.zap.Sugar().Errorf("[CONFIG SERVER ADDON ERROR] %s", err.Error())
			} else {
				println("[CONFIG SERVER ADDON ERROR]", err.Error())
			}
		}

		c <- Result{make([]byte, 0), err}
	}

	opts := &configOptions{}

	for _, op := range options {
		err := op(opts)

		if err != nil {
			c <- Result{make([]byte, 0), err}
			return
		}
	}

	if opts.filePath == "" && opts.fileName == "" {
		errorAbort(fmt.Errorf("please create with a file path or a file name"), c, opts)
	}

	var request *http.Request
	var err error

	if opts.isCtx {
		request, err = http.NewRequestWithContext(opts.ctx, "GET", configServerUrl, nil)
	} else {
		request, err = http.NewRequest("GET", configServerUrl, nil)
	}

	if err != nil {
		errorAbort(err, c, opts)
		return
	}

	query := request.URL.Query()

	if opts.filePath != "" {
		query.Add("filePath", url.QueryEscape(opts.filePath))
	} else {
		if opts.fileName != "" {

			if opts.env != "" {
				opts.fileName += ":" + opts.env
			}

			query.Add("filename", url.QueryEscape(opts.fileName))
		}

		if opts.fileEnding != "" {
			query.Add("gitlabFileEnding", opts.fileEnding)
		}
	}

	request.URL.RawQuery = query.Encode()

	httpClient := &http.Client{}
	resp, err := httpClient.Do(request)

	if err != nil {
		errorAbort(err, c, opts)
		return
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		err = fmt.Errorf("unexpected response code %d", resp.StatusCode)

		errorAbort(err, c, opts)
		return
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errorAbort(err, c, opts)
		return
	}

	jsonResp := gitlabResponseJSON{}
	err = json.Unmarshal(bodyBytes, &jsonResp)
	if err != nil {
		errorAbort(err, c, opts)
		return
	}

	b64Decode, err := base64.StdEncoding.DecodeString(jsonResp.ConfigB64Data)
	if err != nil {
		errorAbort(err, c, opts)
		return
	}

	if opts.isSave {

		filename := opts.fileName

		if opts.filePath != "" {
			_, filename = path.Split(opts.filePath)
			filename = strings.TrimSuffix(filename, path.Ext(filename))
		}

		pathToWrite := opts.savePath + filename + jsonResp.FileEnding

		err = os.MkdirAll(opts.savePath, os.ModePerm)
		if err != nil {
			errorAbort(err, c, opts)
			return
		}

		f, err := os.Create(pathToWrite)
		if err != nil {
			errorAbort(err, c, opts)
			return
		}

		defer f.Close()

		wBytes, err := f.Write(b64Decode)
		if err != nil {
			errorAbort(err, c, opts)
			return
		}

		if opts.isSave && opts.logLevel == INFO {

			if opts.zap != nil {
				opts.zap.Sugar().Infof("[CONFIG SERVER ADDON INFO] Provided PATH: %s", pathToWrite)
				opts.zap.Sugar().Infof("[CONFIG SERVER ADDON INFO] Saved file. Wrote %d Bytes", wBytes)
			} else {
				println("[CONFIG SERVER ADDON INFO] Provided PATH: " + pathToWrite)
				println("[CONFIG SERVER ADDON INFO] Saved file. Wrote " + strconv.Itoa(wBytes) + " Bytes")
			}
		}
	}

	c <- Result{b64Decode, nil}
}
